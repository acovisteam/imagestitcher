function [xs, ys] = FAST(image, thresh = 0.2)

    [rows, columns] = size(image);
  
  xcount = uint16(1);
  ycount = uint16(1);
  
  for i = 1+3:rows-3
    for j = 1+3:columns-3
      checkLighter = zeros(16,1);
      checkDarker = zeros(16,1);
      #TODO code the if different so that it is really either (xor) darker or brighter
      
        #early check upper pixel 1
        if image(i,j+3) > image(i,j) + thresh 
            checkLighter(1,1) = 1;
        end
        if image(i,j+3) < image(i,j) - thresh
            checkDarker(1,1) = 1;
        end
        #early check right pixel 5
        if image(i+3,j) > image(i,j) + thresh 
            checkLighter(2,1) = 1;
        end
        if image(i+3,j) < image(i,j) - thresh
            checkDarker(2,1) = 1;
        end
        #early check lower pixel 9
        if image(i,j-3) > image(i,j) + thresh
            checkLighter(3,1) = 1;
        end
        if image(i,j-3) < image(i,j) - thresh
            checkDarker(3,1) = 1;
        end
        #early check upper pixel 13
        if image(i-3,j) > image(i,j) + thresh
            checkLighter(4,1) = 1;
        end
        if image(i-3,j) < image(i,j) - thresh
            checkDarker(4,1) = 1;
        end
      
      #early check if at least 3 out of 4 point meet the condition
        if sum(checkLighter) >= 3 ||  sum(checkDarker) >= 3
          
            #check pixel 2
            if image(i+1,j+3) > image(i,j) + thresh
                checkLighter(5,1) = 1;
            end  
            if image(i+1,j+3) < image(i,j) - thresh
                checkDarker(5,1) = 1;
            end  
            #check pixel 3
            if image(i+2,j+2) > image(i,j) + thresh
                checkLighter(6,1) = 1;
            end  
            if image(i+2,j+2) < image(i,j) - thresh
                checkDarker(6,1) = 1;
            end  
            #check pixel 4
            if image(i+3,j+1) > image(i,j) + thresh
                checkLighter(7,1) = 1;
            end  
            if image(i+3,j+1) < image(i,j) - thresh
                checkDarker(7,1) = 1;
            end
            #check pixel 6
            if image(i+3,j-1) > image(i,j) + thresh
                checkLighter(8,1) = 1;
            end  
            if image(i+3,j-1) < image(i,j) - thresh
                checkDarker(8,1) = 1;
            end    
            #check pixel 7
            if image(i+2,j-2) > image(i,j) + thresh
                checkLighter(9,1) = 1;
            end
            if image(i+2,j-2) < image(i,j) - thresh
                checkDarker(9,1) = 1;
            end  
            #check pixel 8
            if image(i+1,j-3) > image(i,j) + thresh
                checkLighter(10,1) = 1;
            end  
            if image(i+2,j-2) < image(i,j) - thresh
                checkDarker(9,1) = 1;
            end
            #check pixel 10
            if image(i-1,j-3) > image(i,j) + thresh
                checkLighter(11,1) = 1;
            end  
            if image(i-1,j-3) < image(i,j) - thresh
                checkDarker(11,1) = 1;
            end  
            #check pixel 11
            if image(i-2,j-2) > image(i,j) + thresh
                checkLighter(12,1) = 1;
            end 
            if image(i-2,j-2) < image(i,j) - thresh
                checkDarker(12,1) = 1;
            end  
            #check pixel 12
            if image(i-3,j-1) > image(i,j) + thresh
                checkLighter(13,1) = 1;
            end  
            if image(i-3,j-1) < image(i,j) - thresh
                checkDarker(13,1) = 1;
            end  
            #check pixel 14
            if image(i-3,j+1) > image(i,j) + thresh
                checkLighter(14,1) = 1;
            end  
            if image(i-3,j+1) < image(i,j) - thresh
                checkDarker(14,1) = 1;
            end  
            #check pixel 15
            if image(i-2,j+2) > image(i,j) + thresh
                checkLighter(15,1) = 1;
            end  
            if image(i-2,j+2) < image(i,j) - thresh
                checkDarker(15,1) = 1;
            end  
            #check pixel 16
            if image(i-1,j+3) > image(i,j) + thresh
                checkLighter(16,1) = 1;
            end  
            if image(i-1,j+3) < image(i,j) - thresh
                checkDarker(16,1) = 1;
            end  
              
            if sum(checkLighter) == 12 || sum(checkDarker) == 12 
              xs(xcount) = i;
              ys(ycount) = j;
              xcount = xcount + 1;
              ycount = ycount + 1;
            end
         end
      
      end 
   end
end