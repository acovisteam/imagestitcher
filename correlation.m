function result = correlation(M_input, filter_kernel)
  
  # get dimension of filter kernel
  k_rows = size(filter_kernel, 1); 
  k_cols = size(filter_kernel, 2); 

  # so far only odd template kernel dimensions are supported (lead to symmetric padding)
  assert(mod(k_rows,2)==1);
  assert(mod(k_cols,2)==1);
  
  # get dimension of input
  i_rows = size(M_input,1); 
  i_cols = size(M_input,2); 
  i_chs = size(M_input,3); 
  
  # pad input tensor
  M_padded = zeros(i_rows + k_rows - 1, i_cols + k_cols - 1);
  M_padded(1+(k_rows - 1)/2:i_rows + (k_rows - 1)/2, 1+(k_cols - 1)/2:i_cols + (k_cols - 1)/2) = M_input;
  i_rows = i_rows + k_rows - 1;
  i_cols = i_cols + k_cols - 1;
  
  #create empty matrix
  result = zeros(i_rows - k_rows + 1, i_cols - k_cols + 1, i_chs);

  # iterate over all elements in result matrix
  for ch = 1:i_chs
    for row = 1:i_rows-k_rows+1
      for col = 1:i_cols-k_cols+1
        # each element is result of dot product of filter kernel and belonging inputs
        result(row, col, ch) = sum(sum(M_padded(row:row+k_rows-1, col:col+k_cols-1, ch) .* filter_kernel));
      endfor
    endfor
  endfor  
  
  # if required, get rid of channel dimesnion (in grayscale images)
  if i_chs == 1
    result = reshape(result, [size(result, 1), size(result, 2)]);
  end  
  
end

