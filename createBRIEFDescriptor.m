function result = createBRIEFDescriptor(window, numberPairs, patch)
  bitstring = [];
  for i=1:numberPairs
    
    difference = window(patch(i,1), patch(i,2)) - window(patch(i,3), patch(i,4));
    if(difference <= 0)
      bitstring = [bitstring 0];
    else
      bitstring = [bitstring 1];
    endif
  
  endfor
  result = bitstring;
  
endfunction