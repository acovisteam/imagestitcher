#takes an 18x18 window (including edge regions of the 16x16 window) for gradients
function result = createSIFTDescriptor(image, keypoints, normalize_Threshhold)
  
  result = [];
  window = [];
  [img_row, img_col] = size(image);
  for g = 1:size(keypoints,1)
    
  window = image(max(1,keypoints(g,1)-8):min(img_row,keypoints(g,1)+9),max(1,keypoints(g,2)-8):min(img_col,keypoints(g,2)+9));
  
  [rows, cols] = size(window);
  container = [];
  mag_tmp = zeros(rows-1,cols-1);
  mag = zeros(rows-1,cols-1);
  phase = zeros(rows-1,cols-1);
  #calculate magnitude and phase,  but dont read beyond edge boundaries
  for i=1+1:rows-1
    for j=1+1:cols-1
        mag_tmp(i,j)=((window(i+1,j)-window(i-1,j))^2)+((window(i,j+1)-window(i,j-1))^2);
        phase(i,j)=atan2((window(i,j+1)-window(i,j-1)),(window(i+1,j)-window(i-1,j)));      
        #atan2 apparently has a range of [-pi;pi]
    endfor
  endfor
  mag=sqrt(mag_tmp);

  #make the desciptor
  #divide a 16x16 neighborhood into 4x4 blocks
  for i=1:4:13
    for j=1:4:13
      block_magnitudes = mag(i:i+3,j:j+3);
      block_phases = phase(i:i+3,j:j+3);
      
      # the sizing of these is a little hack, only the 1,5,9,13 of the first two dimensions is assigned a value, due to the block indexing of the i and j loops
      phase_bin = zeros(13,13,8);
      magnitude_bin = zeros(13,13,8);
      ctr = 1;
      for m=1:4
        for n=1:4
            
          # 8 bins, 45 degrees or pi/4 each
          #layout switched from 16x16 to 16x8 with 8 being the phasecount in the particular range of k*(pi/4)
            if block_phases(m,n) > -pi && block_phases(m,n) < -pi*(3/4)
              phase_bin(i,j,1) = phase_bin(i,j,1) + 1;
              magnitude_bin(i,j,1) = magnitude_bin(i,j,1) + block_magnitudes(m,n);
            
            elseif block_phases(m,n) > -pi*(3/4) && block_phases(m,n) < -pi*(2/4)
              phase_bin(i,j,2) = phase_bin(i,j,2) + 1;
              magnitude_bin(i,j,2) = magnitude_bin(i,j,2) + block_magnitudes(m,n);
            
            elseif block_phases(m,n) > -pi*(2/4) && block_phases(m,n) < -pi*(1/4)
              phase_bin(i,j,3) = phase_bin(i,j,3) + 1;
              magnitude_bin(i,j,3) = magnitude_bin(i,j,3) + block_magnitudes(m,n);
            
            elseif block_phases(m,n) > -pi*(1/4) && block_phases(m,n) < 0
              phase_bin(i,j,4) = phase_bin(i,j,4) + 1;
              magnitude_bin(i,j,4) = magnitude_bin(i,j,4) + block_magnitudes(m,n);
            
            elseif block_phases(m,n) > 0 && block_phases(m,n) < pi*(1/4)
              phase_bin(i,j,5) = phase_bin(i,j,5) + 1;
              magnitude_bin(i,j,5) = magnitude_bin(i,j,5) + block_magnitudes(m,n);
            
            elseif block_phases(m,n) > pi*(1/4) && block_phases(m,n) < pi*(2/4)
              phase_bin(i,j,6) = phase_bin(i,j,6) + 1;
              magnitude_bin(i,j,6) = magnitude_bin(i,j,6) + block_magnitudes(m,n);
            
            elseif block_phases(m,n) > pi*(2/4) && block_phases(m,n) < pi*(3/4)
              phase_bin(i,j,7) = phase_bin(i,j,7) + 1;
              magnitude_bin(i,j,7) = magnitude_bin(i,j,7) + block_magnitudes(m,n);
            
            else
              phase_bin(i,j,8) = phase_bin(i,j,8) + 1;
              magnitude_bin(i,j,8) = magnitude_bin(i,j,8) + block_magnitudes(m,n);
            end  
        endfor
      endfor             
      
        for k = 1:8
        tmp = phase_bin(i,j,k)* magnitude_bin(i,j,k);
        container = [container, tmp];
        end
      
    endfor
  endfor
 
 # normalize for luminance invariance
 l1 = sqrt(sum(container.^2));
 if l1 != 0     #safety for division by zero
 container = container / l1;
 endif
 # threshhold normalize to remove particularly high gradients
 container = min(container,normalize_Threshhold);
 # renormalize
 l2 = sqrt(sum(container.^2));
 if l2 != 0     #safety for division by zero
 container = container / l2;
 endif
 result = [result; container];   
endfor 

endfunction