function result = genGaussianKernel(k_size, delta)
  gauss = @(x,y) (1/2*pi*(delta^2))*exp(1)^(-(x.^2 + y.^2)/(2*delta^2));
  
  for row=-((k_size-1)/2):((k_size-1)/2)
    for col=-((k_size-1)/2):((k_size-1)/2)
      integral_sum = 0;
      for y = 0:10
        y2 = (y - 5) / 10;
        for x = 0:10
          x2 = (x - 5) / 10;
          integral_sum = integral_sum + gauss(row + y2,col + x2);
        end
      end      
      
      result(row+((k_size-1)/2)+1,col+((k_size-1)/2)+1) = integral_sum;
    end
  end
  
  result = result / sum(sum(result));
  
endfunction
