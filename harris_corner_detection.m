# Please Note: thresh is actually not a threshold any longer 
function result_vector = harris_corner_detection(imgGray, gaussian_kernel_size, k_factor, thresh)
      ##figure 1;
      ##imshow(imgGray);
  
  # 1. initialize edge sobel filter kernels and filter image

  vertical_edge_filter = [-1 0 1; -2 0 2; -1 0 1];
  horizontal_edge_filter = [-1 -2 -1; 0 0 0; 1 2 1];

  imgFilteredVertically = correlation(imgGray, vertical_edge_filter);
  imgFilteredHorizontally = correlation(imgGray, horizontal_edge_filter);
  
      ##figure 2;
      ##imshow(imgFilteredVertically);
      ##figure 3;
      ##imshow(imgFilteredHorizontally);
  
  # 2. compute products of derivatives
 
  px2 = imgFilteredVertically.^2;
  py2 = imgFilteredHorizontally.^2;
  pxy = imgFilteredVertically.*imgFilteredHorizontally;
  
  # 3. sum up neighbourhood
  
  #sum_kernel = [1 1 1; 1 1 1; 1 1 1];
  sum_kernel = genGaussianKernel(gaussian_kernel_size,1); 
  sum_px2 = correlation(px2, sum_kernel);
  sum_py2 = correlation(py2, sum_kernel);
  sum_pxy = correlation(pxy, sum_kernel);
  
  rows = size(sum_px2,1);
  cols = size(sum_px2,2);
  
      ##figure 4;
      ##imshow(sum_px2);
      ##figure 5;
      ##imshow(sum_py2);
      ##figure 6;
      ##imshow(sum_pxy);  
  
  # 4. calculate harris response

  # consider new packed structured tensor as matrix with:
  # M = [sum_px2  sum_pxy]
  #     [sum_pxy  sum_py2]

  # harris response is defined for each (x,y) in the image as follows:
  #   H(x,y) = sum_px2(x,y) * sum_py2(x,y) - sum_pxy(x,y) * sum_pxy(x,y)  # determinante of M
  #            - k * (sum_px2(x,y) + sum_py2(x,y))^2                      # sum of diagonal and factor k for scaling

  determinant = sum_px2 .* sum_py2 - sum_pxy .* sum_pxy;
  trace_sum = sum_px2 + sum_py2;
  harris_response = determinant - k_factor*(trace_sum.^2);

  # 5. thresholding stage
  # normalize [0 .. 1]
  maxi = max(max(harris_response));
  mini = min(min(harris_response));  
  harris_response = (harris_response - mini) / (maxi - mini);
  mask = (harris_response > 2); #generate only false for size of harris_response
  
  # don't consider the borders of the image-> around each corner, there needs to be a certain field around it, otherwise following algorithms won't work
  mask_tmp = mask(1+9:end-9,1+9:end-9);
  harris_tmp = harris_response(1+9:end-9,1+9:end-9);
  [mrows, mcols] = size(mask_tmp);
  
  # flatten to 1D array
  mask_tmp=mask_tmp(:);
  harris_tmp = harris_tmp(:);
    
  # get k Maximas where k = thresh*rows*cols
  toFind = round(rows*cols*thresh);
  for i = 1:toFind
    [maxVal, maxI] = max(harris_tmp);
    mask_tmp(maxI) = 1;
    harris_tmp(maxI) = -Inf;
  endfor
  mask(1+9:end-9,1+9:end-9) = reshape(mask_tmp, [mrows, mcols]);
  
  harris_response = mask.*harris_response;
  [x,y,v] = find(harris_response);
  targets = [x y];

  # 6. NMS stage
  
  result_vector = [];
  for i = 1:size(targets,1)
    # check if target is the maxima within the region
    r_idx = targets(i,1); 
    c_idx = targets(i,2);  
    # consider the targets 10x10 neighborhood
    region = harris_response(max(1,r_idx-4):min(rows,r_idx+5), max(1,c_idx-4):min(cols,c_idx+5));  
    # get maximum in that region
    max_reg = max(max(region));   
    # check if the target refers to the maxima within that region 
    if max_reg == harris_response(r_idx,c_idx)
      result_vector = [result_vector; [r_idx, c_idx]];
    endif
  endfor
  
endfunction