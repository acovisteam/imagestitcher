function [warp_wrapper] = imtransform(dstImg, srcImg, M, mode=0)     

  dstRows = size(dstImg,1);
  dstCols = size(dstImg,2);
  srcRows = size(srcImg,1);
  srcCols = size(srcImg,2);
  
  #rowoffset = 50;
  
  
  srcImg_ = im2double(srcImg);
  
  # transform image corners
  p1 = M * [1 ; 1 ; 1];
  p2 = M * [srcCols; 1 ; 1];
  p3 = M * [1 ; srcRows; 1];
  p4 = M * [srcCols; srcRows ; 1];
  # normalize points
  p1 = p1 / p1(3);
  p2 = p2 / p2(3);
  p3 = p3 / p3(3);
  p4 = p4 / p4(3);
  # determine boarders of srcImg' (top left and bottom right corner)
  topLeft = [floor(min(p1(1),p3(1))); floor(min(p1(2),p2(2)))];
  botRight = [ceil(max(p2(1),p4(1))); ceil(max(p3(2),p4(2)))];
  # consider as rectangle and sample from source
  warp_wrapper = zeros(max(dstRows,botRight(2)),max(dstCols,botRight(1)),3);
  warp_wrapper(1:dstRows,1:dstCols,:) = im2double(dstImg);
  
  for x=topLeft(1):botRight(1)
    for y=topLeft(2):botRight(2)
      #to get rid of the logical/ integer error (indices where below zero)
      if y >= 1 && x >= 1       
        # transform each point back into srcImage
        t = inv(M) * [x; y; 1];
        t = t / t(3);
        
        # get surrounding pixels for interpolation
        c1 = [floor(t(1)), floor(t(2))];
        c2 = [floor(t(1)), ceil(t(2))];
        c3 = [ceil(t(1)), floor(t(2))];
        c4 = [ceil(t(1)), ceil(t(2))];
        
        # if color idx is out of image, skip
        if c1(1) < 1 || c1(1) > srcCols || c1(2) < 1 || c1(2) > srcRows 
          continue;
        else
          color1 = (srcImg_(c1(2), c1(1), :))(:);
        endif

        if c2(1) < 1 || c2(1) > srcCols || c2(2) < 1 || c2(2) > srcRows 
          continue;
        else
          color2 = (srcImg_(c2(2), c2(1), :))(:);
        endif

        if c3(1) < 1 || c3(1) > srcCols || c3(2) < 1 || c3(2) > srcRows 
          continue;
        else
          color3 = (srcImg_(c3(2), c3(1), :))(:);
        endif      
        if c4(1) < 1 || c4(1) > srcCols || c4(2) < 1 || c4(2) > srcRows 
          continue;
        else
          color4 = (srcImg_(c4(2), c4(1), :))(:);
        endif
        
        # mode == 0: nearest neighbour
        if mode == 0
          #compute distances to c1, c2, c3, c4
          vec = [double(c1(1))-t(1); double(c1(2))-t(2)];
          d1 = sqrt(vec(1)^2 + vec(2)^2);        
          vec = [double(c2(1))-t(1); double(c2(2))-t(2)];
          d2 = sqrt(vec(1)^2 + vec(2)^2);        
          vec = [double(c3(1))-t(1); double(c3(2))-t(2)];
          d3 = sqrt(vec(1)^2 + vec(2)^2);
          vec = [double(c4(1))-t(1); double(c4(2))-t(2)];
          d4 = sqrt(vec(1)^2 + vec(2)^2);
          #select nearest neighbour
          color_vec = [color1'; color2'; color3'; color4'];
          dist_vec = [d1 d2 d3 d4];
          [min_dist, min_idx] = min(dist_vec);
          # write target color        
          tColor = color_vec(min_idx,:);
          
        # mode == 1: bilinear interpolation
        endif
        if mode == 1
          alpha = t(2) - floor(t(2));
          beta = t(1) - floor(t(1));
          
          # first lerps 
          I1 = alpha * color2 + (1 - alpha) * color1;
          I2 = alpha * color4 + (1 - alpha) * color3;
          
          #second lerp between I1 and I2
          tColor = beta * I2 + (1 - beta) * I1;
        endif 
        
        if sum(warp_wrapper(y,x,:)) == 0
          warp_wrapper(y,x,:) = tColor;
        else
          warp_wrapper(y,x,1) = (warp_wrapper(y,x,1) + tColor(1))/2;
          warp_wrapper(y,x,2) = (warp_wrapper(y,x,2) + tColor(2))/2;
          warp_wrapper(y,x,3) = (warp_wrapper(y,x,3) + tColor(3))/2;
        endif
      endif
    endfor
  endfor
endfunction