################################################################################
#                                SETTINGS                                      #         
################################################################################

# folder containing images (need to be named from 0.png, 1.png ... n.png)
# where the i-th image can be stitched onto the (i-1)-th image

tFolder = "./test_img/img1/";

# tartget file ending of files located in tFolder
tFileEnding = "*.png";

# draw border around recently stitched image

VIS_BORDERS = true;

################################################################################
################################################################################

# load files 
files = dir([tFolder tFileEnding]);

# process first image
fprintf ("Processing image: %s...\n", ([files(1).name]));
img0 = [tFolder files(1).name];
img0 = imread(img0);
[rows0, cols0, ~] = size(img0);

# initialize warp wrapper
warp_wrapper = im2double(img0);

# run Harris
fprintf("Harris.. ");
img0Gray = im2double(rgb2gray(img0));
keypoints0 = harris_corner_detection(img0Gray(:,:), 5, 0.04, 0.07);
fprintf( "extracted %d corners.\n", (size(keypoints0,1)));

homography = 0;
# stitch next one onto previous
for img_idx=2:length(files)
  
  fprintf ("Processing image: %s...\n", ([files(img_idx).name]));
  
  # read next image
  img1 = [tFolder files(img_idx).name];
  img1 = imread(img1);
  [rows1, cols1, ~] = size(img1);
  
  # show recent pair
  figure 1;
  wrapper = zeros(max(rows0,rows1), cols0+cols1, 3);
  wrapper(1:rows0,1:cols0,:) = im2double(img0);
  wrapper(1:rows1,cols0:cols0+cols1-1,:) = im2double(img1);
  imshow(wrapper);
  clear wrapper;
  
  # get keypoints in next image
  img1Gray = im2double(rgb2gray(img1));
  fprintf("Harris.. ");
  keypoints1 = harris_corner_detection(img1Gray(:,:), 5, 0.04, 0.07);
  fprintf( "extracted %d corners.\n", (size(keypoints1,1)));
  
  # visualize corners
  figure 2;
  wrapper(1:rows0,1:cols0,:) = img0;
  wrapper(1:rows1,cols0:cols0+cols1-1,:) = img1;
  red_rectangle = uint8(zeros(3,3,3));
  red_rectangle(:,:,1) = uint8(ones(3,3))*255;
  for i = 1:size(keypoints0,1)
    r_idx = keypoints0(i,1);
    c_idx = keypoints0(i,2);
    wrapper(r_idx-1:r_idx+1,c_idx-1:c_idx+1,:) = red_rectangle;
  endfor
  for i = 1:size(keypoints1,1)
    r_idx = keypoints1(i,1);
    c_idx = keypoints1(i,2) + cols0;
    wrapper(r_idx-1:r_idx+1,c_idx-1:c_idx+1,:) = red_rectangle;
  endfor
  imshow(wrapper);
  clear red_rectangle;
  
  # launch SIFT
  disp "SIFT..";
  siftDescriptors0 = createSIFTDescriptor(img0Gray,keypoints0,0.2);
  siftDescriptors1 = createSIFTDescriptor(img1Gray,keypoints1,0.2);

  # launch matching algo  
  disp "Match..";
  matchedPairs = matchFeatures(siftDescriptors0, siftDescriptors1);
  
  # visualize matches
  figure 3;
  imshow(wrapper);
  src_feature_indices = []; 
  dst_feature_indices = [];
  for i = 1:size(matchedPairs,1)
    r1_idx = keypoints0(matchedPairs(i,1),1);
    c1_idx = keypoints0(matchedPairs(i,1),2);
    r2_idx = keypoints1(matchedPairs(i,2),1);
    c2_idx = keypoints1(matchedPairs(i,2),2);
    line([c1_idx (c2_idx + cols0)],[r1_idx r2_idx], "color", "red");
    src_feature_indices = [src_feature_indices; [c2_idx r2_idx]];
    dst_feature_indices = [dst_feature_indices; [c1_idx r1_idx]];     
  endfor
  clear r1_idx;
  clear c1_idx;
  clear r2_idx;
  clear c2_idx;  
  clear wrapper;
  
  # run RANSAC  
  fprintf("Ransac.. ");
  [M,inliers] = ransac(src_feature_indices, dst_feature_indices, 7000, 0.3);
  fprintf("%d inliers\n", inliers);
  if img_idx == 2
    homography = M;
  else
    homography = homography * M;
  endif
  clear M;
  clear src_feature_indices;
  clear dst_feature_indices;
  
  # warp image onto warp_wrapper
  disp "Warp..";
  warp_wrapper = imtransform(warp_wrapper,img1,homography,1);
  figure(4);
  imshow(warp_wrapper);
  fprintf("Press any key to process next Image...\n");
  waitforbuttonpress;
  
  # maybe draw borders
  if VIS_BORDERS
    [r, c, ch] = size(img1);
    p1 = homography * [1 ; 1 ; 1];
    p2 = homography * [c; 1 ; 1];
    p3 = homography * [1 ; r; 1];
    p4 = homography * [c; r ; 1];
    # normalize points
    p1 = p1 / p1(3);
    p2 = p2 / p2(3);
    p3 = p3 / p3(3);
    p4 = p4 / p4(3);
    #draw lines
    line([p1(1) p2(1)],[p1(2) p2(2)], 'LineWidth',1,"color","red");
    line([p1(1) p3(1)],[p1(2) p3(2)], 'LineWidth',1,"color","red");
    line([p2(1) p4(1)],[p2(2) p4(2)], 'LineWidth',1,"color","red");
    line([p3(1) p4(1)],[p3(2) p4(2)], 'LineWidth',1,"color","red");
  endif
  
  # reassign vars
  img0 = img1;
  img0Gray = img1Gray;
  rows0 = rows1;
  cols0 = cols1;
  keypoints0 = keypoints1;
endfor

figure(5);
imshow(warp_wrapper);

fprintf("Done.\n");
clear;