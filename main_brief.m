#load and showimages
img0 = imread("./img/test_imgs/img1/idx0.png");
img0 = rgb2gray(img0);
gauss = genGaussianKernel(9, 2);
img0 = correlation(img0, gauss);

img1 = imread("./img/test_imgs/img1/idx1.png");
img1 = rgb2gray(img1);
img1 = correlation(img0, gauss);

rows0=size(img0,1);
cols0=size(img0,2);
rows1=size(img1,1);
cols1=size(img1,2);

# visualization stuff
figure 1;
visualization_padding = 20;
wrapper(1:rows0,1:cols0,:) = img0;
wrapper(1:rows1,cols0+visualization_padding:cols0+visualization_padding-1+cols1,:) = img1;
imshow(wrapper);

# 1. convert to grayscale

img0Gray = im2double(rgb2gray(img0));
img1Gray = im2double(rgb2gray(img1));

# 2. get keypoints in images (only for a "x-region of interest")
region_size = 100;

keypoints0 = harris_corner_detection(img0Gray(:,cols0-region_size:cols0), 5, 0.06, 0.2);
keypoints1 = harris_corner_detection(img1Gray(:,1:region_size), 5, 0.06, 0.2);

# visualization stuff
figure 2;
red_rectangle = uint8(zeros(3,3,3));
red_rectangle(:,:,1) = uint8(ones(3,3))*255;
for i = 1:size(keypoints0,1)
  r_idx = keypoints0(i,1);
  c_idx = keypoints0(i,2) + cols0 - region_size;
  wrapper(r_idx-1:r_idx+1,c_idx-1:c_idx+1,:) = red_rectangle;
endfor
for i = 1:size(keypoints1,1)
  r_idx = keypoints1(i,1);
  c_idx = keypoints1(i,2) + cols0 + visualization_padding;
  wrapper(r_idx-1:r_idx+1,c_idx-1:c_idx+1,:) = red_rectangle;
endfor
imshow(wrapper);

# 3. describe region around keypoints
#Create Patch
pair_matrix = [];
for i = 1:128
  pixel = [randi(25), randi(25), randi(25), randi(25)];
  pair_matrix = [pair_matrix;pixel];
endfor
disp(pair_matrix);
briefDescriptors0 = [];
for i = 1:size(keypoints0,1)
  r_idx = keypoints0(i,1);
  c_idx = keypoints0(i,2);
  #make SIFT descriptor for local maximum keypoint with threshold value for normalization between [0...1]
  briefDescriptors0 = [briefDescriptors0; createBRIEFDescriptor(image0(max(1,r_idx-15):min(rows0,r_idx+16),max(1,c_idx-15):min(cols0,c_idx+16)),128,pair_matrix)];
  #briefDescriptors0 = [briefDescriptors0, createBRIEF(img0Gray(max(1,r_idx-7):min(rows0,r_idx+8),max(1,c_idx-7):min(cols0,c_idx+8)),randomMatrix(16))];   
endfor
briefDescriptors1 = [];
for i = 1:size(keypoints1,1)
  r_idx = keypoints1(i,1);
  c_idx = keypoints1(i,2);
  #make SIFT descriptor for local maximum keypoint with threshold value for normalization between [0...1]
  briefDescriptors1 = [briefDescriptors1; createBRIEFDescriptor(image1(max(1,r_idx-15):min(rows1,r_idx+16),max(1,c_idx-15):min(cols1,c_idx+16)),128,pair_matrix)];
  #briefDescriptors0 = [briefDescriptors0, createBRIEF(img0Gray(max(1,r_idx-7):min(rows0,r_idx+8),max(1,c_idx-7):min(cols0,c_idx+8)),randomMatrix(16))];   
endfor
disp(briefDescriptors0);

matchedPairs = matchFeatures_BRIEF(briefDescriptors0, briefDescriptors1);

src_feature_indices = []; 
dst_feature_indices = [];

figure 3;
imshow(wrapper);
for i = 1:size(matchedPairs,1)
  r1_idx = keypoints0(matchedPairs(i,1),1);
  c1_idx = keypoints0(matchedPairs(i,1),2) + cols0 - region_size;
  
  r2_idx = keypoints1(matchedPairs(i,2),1);
  c2_idx = keypoints1(matchedPairs(i,2),2);
  
  line([c1_idx (c2_idx + cols0 + visualization_padding)],[r1_idx r2_idx]);
  src_feature_indices = [src_feature_indices; [c2_idx r2_idx]];
  dst_feature_indices = [dst_feature_indices; [c1_idx r1_idx]];     
endfor
