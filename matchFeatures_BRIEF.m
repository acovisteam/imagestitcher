function [matched_pairs] = matchFeatures_BRIEF(briefDescriptors0, briefDescriptors1)     
  matched_pairs = [];

  for i = 1:size(briefDescriptors0,1)
    
    vec0 = briefDescriptors0(i,:);
    errors = [];
    
    #for every vector, compute loss to all other vectors and choose the one with smallest loss as a matched partner
    for j = 1:size(briefDescriptors1,1)
      vec1 = briefDescriptors1(j,:);
      
      diff = xor(vec0,vec1);
      
      hamming = sum(diff);
      disp(hamming);
      errors = [errors; hamming];
    endfor
    
    [lowest_loss, partner] = min(errors);
    kombi = [i partner lowest_loss]; 
    setVar = 0;
    
    for j = 1:size(matched_pairs,1)
      a = matched_pairs(j,1);
      b = matched_pairs(j,2);
      l = matched_pairs(j,3);
      
      if kombi(1) == a || kombi(2) == b 
        if kombi(3) <= l
          matched_pairs(j,:) = kombi;
          setVar = 1;
        endif
        setVar = 1;
      endif
    endfor
    
    if setVar == 0
      matched_pairs = [matched_pairs; kombi];
    endif
  endfor
endfunction