function result = randomMatrix(matrixSize)
  mSize = matrixSize;
  array = randperm(mSize*mSize);
  for i=0:mSize-1
    for j=0:mSize-1
      result(i+1,j+1) = array(1,(i*mSize)+ j +1); 
    endfor
  endfor
endfunction