function [transformation, cnt_inliers] = ransac(src_points, dst_points, iters, thresh)     

  cnt_inliers = 0;
  nMatches = length(src_points);

  for i = 1:iters
    
      #randomly sample minimum cobinations required to solve linear least squares
      rndIndices = randperm(nMatches);
      
      # esitmate motion model
      tmp_src = src_points(rndIndices(1:4),:);
      tmp_dst = dst_points(rndIndices(1:4),:);

      # create matrix A
      A = [];
      for i = 1:length(tmp_src)
          src_hom = [tmp_src(i,:)';1];
          dst_x = tmp_dst(i,1);
          dst_y = tmp_dst(i,2);
          A = [A; [  src_hom'    0 0 0      -dst_x*src_hom']];
          A = [A; [   0 0 0     src_hom'    -dst_y*src_hom']];
      end
      
      [~,~,V]=svd(A); 
      H = V(:,size(V,2));
      H = reshape(H, 3, 3)';
      
      # calculate loss when transforming other points
      tmp_src = src_points(rndIndices(5:nMatches),:);
      tmp_src = [tmp_src'; ones(1,length(tmp_src))];
      tmp_dst = H*tmp_src;
      predict(:,1) = (tmp_dst(1,:)./tmp_dst(3,:))';
      predict(:,2) = (tmp_dst(2,:)./tmp_dst(3,:))';
      
      loss = sum((dst_points(rndIndices(5:nMatches),:) - predict).^2,2);
      
      #compare loss against consensus treshold to count inliers
      inlier_idx = find(loss < thresh);
      tmp_cnt_inliers = length(inlier_idx);
      
      # if a better solution for H was found, save result
      if tmp_cnt_inliers > cnt_inliers
          transformation = H;
          cnt_inliers = tmp_cnt_inliers;
      end
  end
endfunction