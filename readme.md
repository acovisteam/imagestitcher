# Image Stitching Project
by Christoph Doehring, Luis Eichhorn and Max Piontek

## Run the Stitching Pipeline

In order to process images through the pipeline:

1. Copy images to be stitched to ./test_img/<any_subfolder>/
	- the images need to be named like 0.png, 1.png, ..., n.png
	- make sure that the i-th image can be stitched onto the (i-1)-th image
2. Open main.m and set variables:
	- set `tFolder` to your target folder path containing the images
	- set `tFileEnding` to *.<your_endings>
3. Run `main.m` (tested in Octave)

Five figures will be opened:
	- Figure 1 showing the currently stitched pair
	- Figure 2 showing the detected keypoints in that image
	- Figure 3 showing the matches
	- Figure 4 showing the warped image(s)
	#### (Make sure to click on Figure 4, before the algorithm will continue with next image)
	- Figure 5 showing the final stitching result

## Results
	
	- check ./test_img/<any_subfolder>/<result> to review the stitching results that were created with the provided source during this project

## Source Files

### Main Porgram:
	- `main.m`
	- `main_brief.m` (stitching pipeline implemented with BRIEF)
### Keypoint Detectors:
	- `harris_corner_detection.m`
	- `FAST.m`
### Keypoint Descriptors:
	- `createBRIEFDescriptor.m``
	- `createSIFTDescriptor.m`
### Matching:
	- `matchFeatures.m` (square error as metric)
	- `matchFeatures_BRIEF.m` (Hamming Distance as metric)
### Model Estimation:
	- `ransac.m`
### Warping:
	- `imtransform.m`
### Utility Functions:
	- `correlation.m`
	- `rgb2gray.m`
	- `randomMatrix.m`
	- `genGaussianKernel.m`

