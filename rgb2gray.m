# convert image to grayscale

function result = rgb2gray(M_input)
  #transform input to [3, rows, cols]
  tmp = permute(M_input, [3,1,2]);
  
  #seperate color channels
  r = tmp(1,:,:);
  g = tmp(2,:,:);
  b = tmp(3,:,:);
  
  # luminosity/weighted method from https://www.tutorialspoint.com/dip/grayscale_to_rgb_conversion.htm
  result = uint8((0.3 * r + 0.59 * g + 0.11 * b));
  # reshape array from [1, rows, cols] to [rows, cols]
  result = reshape(result, [size(result,2), size(result,3)]);
endfunction
